import threading

from flask import Flask
from flask import request
from bson import json_util

import recommender_system
from parser_data import Parser
from __init__ import app, db
from models import Event
recommendation = recommender_system.Reccomendation_system()
recommendation.initialization_matrix(Event.objects().to_json())


@app.route('/events', methods=['POST'])
def add_events():
    events = request.get_json()
    for o in Parser.deserialize(events):
        if Event.objects(event_id=o.event_id).first() is None:
            o.save()
        else:
            mark = Event.objects(event_id=o.event_id).first()
            mark.remove()
            o.save()
    recommendation.initialization_matrix(Event.objects().to_json())
    return str(events)


@app.route('/events', methods=['DELETE'])
def delete_events():
    events = request.get_json()
    for o in Parser.deserialize(events):
        mark = Event.objects(event_id=o.event_id).first()
        mark.remove()
    recommendation.initialization_matrix(Event.objects().to_json())
    return str(events)


@app.route('/update_all', methods=['POST'])
def update_all():
    events = request.get_json()
    Event.objects.delete()
    for o in Parser.deserialize(events):
        o.save()
    recommendation.initialization_matrix(Event.objects().to_json())
    return str(events)


@app.route('/get_reccomend', methods=['POST'])
def get_reccomend():
    events = request.get_json()
    result = []
    for o in Parser.deserialize(events):
        for i in recommendation.recommend(o.id, 5): result.append(Event.objects(event_id=i).first().to_json())
    result = list(set(result))
    return str(result)


@app.route('/', methods=['GET'])
def simple_get():
    return 'get'

def initialization_reccomend():
    threading.Thread(target=recommendation.initialization_matrix, args=(Event.objects().to_json(),))



if __name__ == '__main__':
    app.run()

