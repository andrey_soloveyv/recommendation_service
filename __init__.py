from flask import Flask
from flask_mongoengine import MongoEngine
import os

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = os.urandom(24)
# app.config['SERVER_NAME'] = '83.139.147.40:5000'
app.config['MONGODB_SETTINGS'] = {
    'db': 'recommendation',
    'host': 'localhost',
    'port': 27017
}

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = MongoEngine(app)
# db.init_app(app)
