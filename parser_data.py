import pandas as pd
import json
import models


class Parser:
    @staticmethod
    def create_csv_file(json):
        df = pd.read_json(json, encoding='utf-8')
        return df.to_csv(r'test.csv', encoding='utf-8', index=None)


    @staticmethod
    def deserialize(jsonData):
        objects = []
        data = jsonData
        for o in data:
            objects.append(
                models.Event(event_id=o['event_id'],brief_description= o['brief_description'], full_description=o['full_description'],date_time= o['date_time'], place=o['place']))
        return objects
# [{
# "event_id": 0,
# "date_time": "string",
# "place": "string",
# "brief_description": "string",
# "full_description": "string"
# } ]
