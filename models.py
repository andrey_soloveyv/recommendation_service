import mongoengine as me


class Event(me.Document):
    event_id = me.IntField(primary_key=True)
    brief_description = me.StringField()
    full_description = me.StringField()
    date_time = me.StringField()
    place = me.StringField()

    def to_json(self, *args, **kwargs):
        return super().to_json(*args, **kwargs)


